/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionpostgres;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author ize
 */
public class ConexionPostgres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Link del driver https://jdbc.postgresql.org/download.html

        //Ruta del driver y de la BD, ademas de especificar la BD
        String db = "jdbc:postgresql://127.0.0.1:5432/conJava";

        try {
            //Creacion de objeto de tipo connection,pasando el get para pasar
            //la ruta del driver y BD, ademas de pasarle el nombre y la contraseña
            Connection conectar = DriverManager.getConnection(db,"postgres","");

            //Mandamos a imprimir en caso de que sea correcto.
            //JOptionPane.showMessageDialog(null, "Exito al conectar con la BD");

            

            //Creacion de la sentecia SQL
            String sentenciaSql = "SELECT * FROM public.test_db";
            
            //Creacion del objeto instruccion del tipo sentencia
            Statement instruccion = conectar.createStatement();
            // Creacion del objeto resultado
            ResultSet resultado = instruccion.executeQuery(sentenciaSql);
          
            if(resultado.next()) {
                System.out.println(resultado.getInt("id"));
            }
        } catch (SQLException e) {
            System.out.println("Error: "+ e );
            //JOptionPane.showMessageDialog(null, "Error: " + e);
        }
    }

}
